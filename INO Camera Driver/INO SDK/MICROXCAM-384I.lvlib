﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+C!!!*Q(C=\:;`DN.!%)&gt;`"UD1ZAV12*@CCHG&amp;F#=,)?56ZB838JHW"!C./)E_$:W&lt;&amp;,T!6.?HADKPM(R?L_^0E2R))&amp;VROR\(_ZO&gt;G=`LD77JND@3+Z8(GE[V"@ZXR^PC:P#8PO],P3`6[L"5[8LUFW%]THBQ+N&gt;FD*^[X_+H9`,X`?HY_`8\O[N7PT`;KL_\0NZO4N^`.^Z@VX7&amp;XJ6K&gt;6CK..X`-"ZH0$B.`.X5OR9`(:/`[U\(X[`@X6WV_NX2^OR`!P\&amp;[@WXO$H6`O4P@@HIJ0`=J$-.,S)MM=!=-QWOEOC*HOC*HOC*(OC"(OC"(OC"\OC/\OC/\OC/&lt;OC'&lt;OC'&lt;OC'8DO[U)5O&gt;&amp;9F+:Y53J)G#:,*I#AZ*4Q*4]+4]$"5QJ0Q*$Q*4],$&amp;#5]#5`#E`!E0)1JY5FY%J[%*_%B638*WN(B38B)LY!HY!FY!J[!BZ)+?!+!I&amp;C1/%A#BA*H="(Q"$Q"$Z=+?!+?A#@A#8BQ+_!*?!+?A#@A);3O3F3;I;0$1RIZ0![0Q_0Q/$SEFM0D]$A]$I`$1TEZ0![0!_%5&gt;*+$)#@)G?!-("[(BR]Z0![0Q_0Q/$SY[B0SOD)$T&gt;$2Y4&amp;Y$"[$R_!R?%ABA]@A-8A-(I/(N$*Y$"[$R_!R?#AFA]@A-8A-%+-IZ75E-Q+.39:A]($5X7,V+55FM&gt;KFNHH6.K8;:F0&lt;2'K&lt;1_WBKTV-N9?ENPBKC[KW7'K,I(:T;N"K-'J&amp;V)+(C4JQXG-\&lt;)NNM$7WQJ&lt;9(*M.I@^YYO&amp;QU([`VW[XUX;\V7;TU8K^VGKVUH+ZV(Q_VWQWO`W=?%M@W^HU8LLE'HNZ&gt;@(T`/O(4^]`P``VZ9L@KYO0Z_CPPYVTDPL`VI:]0W\@3U`BX;A8+P=_QVCDXV;VMI5!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="subvi" Type="Folder">
		<Item Name="Error Converter (ErrCode or Status).vi" Type="VI" URL="../subvi/Error Converter (ErrCode or Status).vi"/>
	</Item>
	<Item Name="fn De Initialize.vi" Type="VI" URL="../VIs/fn De Initialize.vi"/>
	<Item Name="fn Error To Text.vi" Type="VI" URL="../VIs/fn Error To Text.vi"/>
	<Item Name="fn Get Camera Image Offset.vi" Type="VI" URL="../VIs/fn Get Camera Image Offset.vi"/>
	<Item Name="fn Get Camera Image.vi" Type="VI" URL="../VIs/fn Get Camera Image.vi"/>
	<Item Name="fn Get DAC Fixed Value And State.vi" Type="VI" URL="../VIs/fn Get DAC Fixed Value And State.vi"/>
	<Item Name="fn Get External Trigger.vi" Type="VI" URL="../VIs/fn Get External Trigger.vi"/>
	<Item Name="fn Get Factory Info.vi" Type="VI" URL="../VIs/fn Get Factory Info.vi"/>
	<Item Name="fn Get FPA Bias.vi" Type="VI" URL="../VIs/fn Get FPA Bias.vi"/>
	<Item Name="fn Get FPA Temp.vi" Type="VI" URL="../VIs/fn Get FPA Temp.vi"/>
	<Item Name="fn Get Gain.vi" Type="VI" URL="../VIs/fn Get Gain.vi"/>
	<Item Name="fn Get Image Size.vi" Type="VI" URL="../VIs/fn Get Image Size.vi"/>
	<Item Name="fn Get Lsync Pw.vi" Type="VI" URL="../VIs/fn Get Lsync Pw.vi"/>
	<Item Name="fn Get SHCDSPW And Start.vi" Type="VI" URL="../VIs/fn Get SHCDSPW And Start.vi"/>
	<Item Name="fn Get Shutter Position.vi" Type="VI" URL="../VIs/fn Get Shutter Position.vi"/>
	<Item Name="fn Get SHVIDPW And Start.vi" Type="VI" URL="../VIs/fn Get SHVIDPW And Start.vi"/>
	<Item Name="fn Get Software Info.vi" Type="VI" URL="../VIs/fn Get Software Info.vi"/>
	<Item Name="fn Get Status And Error.vi" Type="VI" URL="../VIs/fn Get Status And Error.vi"/>
	<Item Name="fn Initialize.vi" Type="VI" URL="../VIs/fn Initialize.vi"/>
	<Item Name="fn Load RPL File.vi" Type="VI" URL="../VIs/fn Load RPL File.vi"/>
	<Item Name="fn Save Factory Param.vi" Type="VI" URL="../VIs/fn Save Factory Param.vi"/>
	<Item Name="fn Set DAC Fixed Value And State.vi" Type="VI" URL="../VIs/fn Set DAC Fixed Value And State.vi"/>
	<Item Name="fn Set External Trigger.vi" Type="VI" URL="../VIs/fn Set External Trigger.vi"/>
	<Item Name="fn Set FPA Bias.vi" Type="VI" URL="../VIs/fn Set FPA Bias.vi"/>
	<Item Name="fn Set Gain.vi" Type="VI" URL="../VIs/fn Set Gain.vi"/>
	<Item Name="fn Set Lsync Pw.vi" Type="VI" URL="../VIs/fn Set Lsync Pw.vi"/>
	<Item Name="fn Set RAW Process Option.vi" Type="VI" URL="../VIs/fn Set RAW Process Option.vi"/>
	<Item Name="fn Set SHCDSPW And Start.vi" Type="VI" URL="../VIs/fn Set SHCDSPW And Start.vi"/>
	<Item Name="fn Set Shutter Position.vi" Type="VI" URL="../VIs/fn Set Shutter Position.vi"/>
	<Item Name="fn Set SHVIDPW And Start.vi" Type="VI" URL="../VIs/fn Set SHVIDPW And Start.vi"/>
	<Item Name="fn Take Offset.vi" Type="VI" URL="../VIs/fn Take Offset.vi"/>
</Library>
